# Python worker template

This repository is the official template to create a MCAI Python worker.

Some adaptations should be made when developing your own worker (such as naming or CI configuration), however the project structure and packaging should follow the given guidelines.

## General guidelines

Developing a worker implies that you follow some guidelines, otherwise it might not be able to operate correctly with MCAI's architecture:

- Edit the [`pyproject.toml`](pyproject.toml) file and replace:
  - `name`: declare the name of the worker.
  - `version`: manage the version of the worker here.
  - `license`: select the license of the worker (can be Private, Commercial or any [SPDX license](https://spdx.org/licenses/)).
  - `description`: redact a short description of what the worker does.
  - Any other fields supported by `pyproject.toml` can be added too.
  - To use the media SDK, replace the dependency with [mcai-worker-sdk-media](https://pypi.org/project/mcai-worker-sdk-media/) and follow instructions from the step 3 to adapt the [worker.py](py_mcai_worker/worker.py) file.


- Edit the [worker.py](py_mcai_worker/worker.py)
  - Update McaiWorkerParameters class and adapt input parameters of the worker.
  - Implement McaiWorker class methods as described in the docs:
    - [Documentation SDK](https://media-cloud-ai.gitlab.io/sdks/py_mcai_worker_sdk/mcai_worker_sdk/v2.0.0-rc7/)
    - [Documentation SDK Media](https://media-cloud-ai.gitlab.io/sdks/py_mcai_worker_sdk/mcai_worker_sdk_media/v2.0.0-rc7/)


- Configure the Continuous Integration (CI) file to build the Docker image of the worker if necessary.

*The `pyproject.toml` file is comptabile with several Python build backends (including Poetry for instance). However, unless you have very special needs not supported yet, we recommend using [Setuptools](https://setuptools.pypa.io/en/latest/) and [Wheel](https://wheel.readthedocs.io/en/latest/).*

## Set up the development environment

In a virtualenv:

```bash
# This will fetch and install dependencies referenced in the pyproject.toml
pip install .
```

Edit the code and start your worker with:

```
python py_mcai_worker/worker.py
```

You can set a source order (*i.e.* a dummy job that will be executed by the worker) by setting the environment variable `SOURCE_ORDERS` to the path of the source order file.


## Install and run the worker

Please refer to the [SDK documentation](https://media-cloud-ai.gitlab.io/sdks/py_mcai_worker_sdk/mcai_worker_sdk/v2.0.0-rc7/) to get information about runtime configuration.

### Locally

In a virtualenv:

```bash
pip install .
py_mcai_worker
```

The command line name (`py_mcai_worker`) depends on the key (left parameter) you gave in the [pyproject.toml `project.script` section](pyproject.toml#L16).

### Docker

```bash
docker build -t py_mcai_worker .
docker run --rm py_mcai_worker
```

You may have to configure additional flags in the `docker run` command line, such as environment variables for connecting to RabbitMQ or source orders.

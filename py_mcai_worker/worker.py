"""
MCAI Python Worker
Basic structure of a Media Cloud AI worker.
"""
import logging

import mcai_worker_sdk as mcai


class McaiWorkerParameters(mcai.WorkerParameters):
    """
    Worker parameters class
    This class defines the input parameters of each job processed by the worker.
    They need to be explicitly typed to allow the SDK to type them properly when a job arrives.
    """

    source_path: str
    destination_path: str


class McaiWorker(mcai.Worker):
    """
    Worker class
    This is the implementation of your worker. It includes several methods that can be optional.
    """

    def setup(self) -> None:
        """
        Optional worker setup function. May be used to load models, do some checks...
        """
        logging.info("Setting up the worker")

    def process(
        self, channel: mcai.McaiChannel, _parameters: McaiWorkerParameters, job_id: int
    ):
        """
        Standard worker process function.
        """
        logging.info("Processing the job with ID: %s", job_id)
        channel.set_job_status(mcai.JobStatus.Completed)


def main():
    """
    This function must be defined. It will be the entrypoint of your worker
    """
    description = mcai.WorkerDescription(__package__)
    worker = McaiWorker(McaiWorkerParameters, description)
    worker.start()


if __name__ == "__main__":
    main()

FROM python:3.9

COPY pyproject.toml /src/
ADD py_mcai_worker /src/py_mcai_worker
WORKDIR /src

RUN pip install .

ENV AMQP_QUEUE=job_py_mcai_worker
ENV MCAI_LOG=info

# The name of the executable depends on the key put in the pyproject [project.scripts] section
CMD py_mcai_worker 
